--make sure chain is not in db
SELECT *
FROM public."chain"
WHERE public."chain".name iLIKE 'fleet feet';

--insert fleet feet chain into chain table
INSERT INTO chain(name)
VALUES ('Fleet Feet');

--find the id for the SubPersona 'shoe_store'
SELECT *
FROM public."SubPersona"
WHERE name ilike 'shoe%';

-- look at other chains with the shoe_store sub_persona_id to verify accuracy
SELECT *
FROM chain_sub_persona
WHERE sub_persona_id = --887705686828386215;

SELECT *
FROM chain
WHERE id= --1954552629262550334;

--associate the chain with the 'shoe store' sub persona
INSERT INTO chain_sub_persona(chain_id, sub_persona_id)
VALUES --(1954552629262550334, 887705686828386215);

--check to make sure record was entered
SELECT *
FROM chain_sub_persona
WHERE chain_id = --1954552629262550334

--check place_google table for '%fleet feet%'
SELECT name, chain_id
FROM place_google
WHERE name ILIKE '%fleet%feet%'

--add chain id to place_google table
UPDATE place_google
SET chain_id = --1954552629262550334
WHERE name IN (SELECT pg.name
              FROM ( SELECT name
                      FROM place_google
                      WHERE name ILIKE '%fleet%feet%') pg
              WHERE name!='Fleet Feet Lifestyle Womens Boutique');

--check place_neustar table for '%fleet feet%'
SELECT name, chain_id
FROM place_neustar
WHERE name ILIKE '%fleet%feet%'

--add chain id to place_neustar table
UPDATE place_neustar
SET chain_id = --1954552629262550334
WHERE name IN (SELECT pn.name
              FROM ( SELECT name
                      FROM place_neustar
                      WHERE name ILIKE '%fleet%feet%') pn
              WHERE name!='Fleet Feet Lifestyle Womens Boutique');

--check place_custom table for '%fleet feet%'
SELECT name, chain_id
FROM place_custom
WHERE name ILIKE '%fleet%feet%'

--add chain id to place_custom table
UPDATE place_custom
SET chain_id = --1954552629262550334
WHERE name IN (SELECT pn.name
              FROM ( SELECT name
                      FROM place_custom
                      WHERE name ILIKE '%fleet%feet%') pn
              WHERE name!='Fleet Feet Lifestyle Womens Boutique');

--check place
SELECT *
FROM place
WHERE name ILIKE '%fleet%feet%';

--confirm that the place without the chainId in place is from place_custom
SELECT *
FROM place_custom
WHERE id = --1439992435236144447;